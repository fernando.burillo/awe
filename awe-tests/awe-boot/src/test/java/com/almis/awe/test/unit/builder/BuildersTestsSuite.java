package com.almis.awe.test.unit.builder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses({
  ScreenBuilderTest.class,
  ClientActionBuilderTest.class
})
@RunWith(Suite.class)
public class BuildersTestsSuite {
}

