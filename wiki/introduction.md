Almis Web Engine > **[Home](../README.md)**

---

# **Introduction**

## Table of Contents

* **[Purpose](purpose.md)**
* **[Architecture](architecture.md)**
* **[Frameworks](frameworks.md)**
* **[Requirements](requirements.md)**
