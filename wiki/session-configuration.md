Almis Web Engine > **[Configuration](configuration-guide.md)**

---

# **Session configuration**

## Table of Contents

* **[Introduction](#introduction)**
* **[Basic session](#basic-session)**
* **[Spring session](#spring-session)**
  * **[Single tab session](#single-tab-session)**
  * **[Shared session](#shared-session)**

---

## Introduction

AWE allows developers to configure the application session in some ways: Using the default `HttpSession` (Basic session) 
deployed by the application server or using Spring Session instead.

## Basic session

AWE is configured by default to use a basic application server `HttpSession` object. So you don't have to do anything to
configure this option. This kind of session is very useful when you deploy your application on a clustered environment
and the balancer server uses the `JSESSIONID` cookie generated by the application server.

## Spring session

Spring session allows the application to be deployed using a shared session server storage, such as REDIS, MONGO, HAZELCAST or
even JDBC connections. To activate the usage of spring session in your application simply add the following depencency
to your `pom.xml` file:  

```xml
<!-- Spring session -->
<dependency>
  <groupId>org.springframework.session</groupId>
  <artifactId>spring-session</artifactId>
</dependency>
```

The configuration of spring session is defined on `session.properties`. These properties are defined on [properties configuration page](properties.md#session-properties).

### Single tab session

Your application can be configure to show different sessions in each browser tab. Ensure your application is working 
with [Spring Session](#spring-session) and configure the property `session.shareSessionInTabs=false`.

### Shared tab session

You can define a shared tab session using [Spring Session](#spring-session) or [Basic Session](#basic-session). For the 
first one, just set the property `session.shareSessionInTabs` to `true`. With basic session profile all tabs share the
same session by default.